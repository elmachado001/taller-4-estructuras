package Practica4;

import edu.princeton.cs.algs4.*;

import java.util.Comparator;

public class Cancion{

    private String artistname, releasename, songhotttnesss,title, location;
    private String year;

    Cancion(){
        this.artistname = "";
        this.releasename = "";
        this.songhotttnesss = "";
        this.title = "";
        this.year = "";
        this.location = "";
    }

    public Cancion(String artistname, String releasename, String songhotttnesss, String title, String year, String location) {
        this.artistname = artistname;
        this.releasename = releasename;
        this.songhotttnesss = songhotttnesss;
        this.title = title;
        this.year = year;
        this.location = location;
    }

    public String getArtistname() {
        return artistname;
    }

    public String getReleasename() {
        return releasename;
    }

    public String getSonghotttnesss() {
        return songhotttnesss;
    }

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public String getLocation() {
        return location;
    }

    public String  toString(){

       return "| Titulo: "+this.title+" "+"| Nombre del Artista: "+this.artistname+"| Release Name: "+this.releasename+"| songhotttnesss: "+this.songhotttnesss+"| Año: "+this.year+"| Locacion: "+this.location;


    }



   public class ColeccionCanciones{
        Queue LeerCanciones(){
            In csv = new In("file:src/archivo/music_1.csv");
            Queue listaDeCanciones = new Queue();
            String linea;
            for(int i=1; i<=50;i++) {
                linea = csv.readLine();
                String[] tokens = linea.split(",");
                listaDeCanciones.enqueue(new Cancion(tokens[2], tokens[21], tokens[23], tokens[33], tokens[34], tokens[15]));

            }
            return listaDeCanciones;
        }
    }

    class SortbyArtistname implements Comparator<Cancion> {
        public int compare(Cancion a, Cancion b)
        {
            return a.artistname.compareTo(b.artistname);
        }
    }
    class SortbyTitle implements Comparator<Cancion> {
        public int compare(Cancion a, Cancion b)
        {
            return a.title.compareTo(b.title);
        }
    }
    class SortbyLocation implements Comparator<Cancion> {
        public int compare(Cancion a, Cancion b)
        {
            return a.location.compareTo(b.location);
        }
    }
    class SortbyYear implements Comparator<Cancion> {
        public int compare(Cancion a, Cancion b)
        {
            return a.year.compareTo(b.year);
        }
    }



}
